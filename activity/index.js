const registeredUsers = [
	'Jeff',
	'Britta',
	'Abed',
	'Annie',
	'Troy',
	'Shirley',
	'Pierce',
];

const friendList = [];

const register = function (user) {
	if (registeredUsers.includes(user)) {
		alert('User already registered');
	} else {
		registeredUsers.push(user);
	}
};

const addFriend = function (user) {
	if (!registeredUsers.includes(user)) {
		alert('User is not yet registered');
	} else {
		friendList.push(user);
	}
};

const displayFriends = function () {
	if (!friendList.length) {
		alert('No user in your friend list');
	} else {
		friendList.forEach((friend) => console.log(friend));
	}
};

const displayFriendNumber = function () {
	if (!friendList.length) {
		alert('No user in your friend list');
	} else {
		console.log(friendList.length);
	}
};

const deleteFriend = function () {
	friendList.pop();
};

console.log(registeredUsers);
register('Jeff');
register('Chang');

console.log(registeredUsers);
addFriend('Dean');
addFriend('Abed');

displayFriends();

displayFriendNumber();
